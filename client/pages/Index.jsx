import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import React from 'react';
import styled from 'styled-components';
import { Button } from '../components/Button';
import { Posts } from '../../imports/collections/Posts';

export const HelloPage = (props) => <h1>Hello, world! {props.count || 'X'}</h1>;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  background: ${props => props.color};
  width: 100%;
  height: 100%;
  overflow-y: hidden;
`;

class HogeComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: '',
    };
  }
  componentDidMount() {
  }
  componentWillUnmount() {
  }
  render() {
    console.log(this.props);
    return (
      <Container color="#FAFAFA">
        <h1>
          Hello, world!
        </h1>
        <div>{this.state.count}</div>
        {
          (this.props.posts || []).map(post => {
            return <div>{post.text}</div>
          })
        }
        <input
          value={this.state.text}
          onChange={(event) => this.setState({text: event.target.value})}
        />
        <Button
          onClick={() => {
            Meteor.call('Posts.Add', this.state.text, (err, res) => {
              this.setState({text: ''});
              console.log(err, res);
            });
          }}
        >くりっく！</Button>
      </Container>
    );
  }
}

export const HogePage = withTracker(({ id }) => {
  Meteor.subscribe('Posts.FindAll');
  return {
    posts: Posts.find({}, {sort: {createdAt: 1}}).fetch(),
  };
})(HogeComponent);
