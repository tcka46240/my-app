import { Meteor } from 'meteor/meteor';
import { Monogo } from 'meteor/mongo';

export const Posts = new Mongo.Collection('posts');
if (Meteor.isServer) {
  Posts.rawCollection().createIndex({ createAt: 1 });   
}