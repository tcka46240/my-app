import { Meteor } from 'meteor/meteor';
import { Posts } from '../../imports/collections/Posts';
import moment from 'moment';

Meteor.publish('Posts.FindAll', function() {
  return Posts.find({});
});

Meteor.methods({
  'Posts.Add'(text) {
    const now = moment().toDate();
    return Posts.insert({ text: text, createdAt: now, updatedAt: now });
  }
});